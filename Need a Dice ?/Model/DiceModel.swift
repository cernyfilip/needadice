//
//  DiceModel.swift
//  Need a Dice ?
//
//  Created by Filip Cerny on 31/05/2018.
//  Copyright © 2018 Blind Chameleon Studio. All rights reserved.
//

import Foundation

// MARK: Dice Type enum

enum DiceType {

    case Dx4, Dx6, Dx8, Dx10, Dx12, Dx14, Dx16, Dx20, DxPercent
    
    
    func getDice() -> (String, Int, Array<String>){
        var name : String!
        var numberOfSides : Int!
        var arrayOfDiceImages : Array<String>!
        
        switch self {
            
        case .Dx4 :
            name = "Dx4"
            numberOfSides = 4
            arrayOfDiceImages = [""]
        case .Dx6 :
            name = "Dx6"
            numberOfSides = 6
            arrayOfDiceImages = [""]
        case .Dx8 :
            name = "Dx8"
            numberOfSides = 8
            arrayOfDiceImages = [""]
        case .Dx10 :
            name = "Dx10"
            numberOfSides = 10
            arrayOfDiceImages = [""]
        case .Dx12 :
            name = "Dx12"
            numberOfSides = 12
            arrayOfDiceImages = [""]
        case .Dx14 :
            name = "Dx14"
            numberOfSides = 14
            arrayOfDiceImages = [""]
        case .Dx16 :
            name = "Dx16"
            numberOfSides = 16
            arrayOfDiceImages = [""]
        case .Dx20 :
            name = "Dx20"
            numberOfSides = 20
            arrayOfDiceImages = [""]
        case .DxPercent :
            name = "DxPercent"
            numberOfSides = 10
            arrayOfDiceImages = [""]
        }
        return (name, numberOfSides, arrayOfDiceImages)
    }
    
    
}


