//
//  DiceVc.swift
//  Need a Dice ?
//
//  Created by Filip Cerny on 31/05/2018.
//  Copyright © 2018 Blind Chameleon Studio. All rights reserved.
//

import UIKit

class DiceVC: UIViewController, ChosenDicesDelegate {
    
    func userSelectDice(dices: Array<DiceType>) {
        print(dices)
    }
    
    
    var imageView0 : UIImageView!
    var imageView1 : UIImageView!
    let image0 = UIImage(named: "D6xBlackDice1")
    let image1 = UIImage(named: "D6xRedDice1")
    
    // MARK: MOCK VALUES, DELETE!!!!
    let mockArray : Array = ["0", "1", "2"]
    
    override func viewDidLoad() {

        imageView0 = UIImageView(image: image0)
        imageView0.frame = CGRect(x : 65, y : 50, width : 100, height : 100)
        view.addSubview(imageView0)
        
        imageView1 = UIImageView(image: image1)
        imageView1.frame = CGRect(x : 215, y : 50, width : 100, height : 100)
        view.addSubview(imageView1)
        
        createViewByNumberOfDices()
        
    }

    func createViewByNumberOfDices() {
        
        if mockArray.count == 1 {
            //print("Creating view for one dice")
        } else if mockArray.count == 2 {
            //print("Creating view for two dice")
        } else if mockArray.count == 3 {
            //print("Creating view for three dices")
        } else if mockArray.count == 4 {
            //print("Creating view for four dices")
        } else if mockArray.count == 5 {
            //print("Creating view for five dices")
        } else if mockArray.count == 6 {
            //print("Creating view for six dices")
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
