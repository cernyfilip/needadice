//
//  DiceCatalogVC.swift
//  Need a Dice ?
//
//  Created by Filip Cerny on 01/06/2018.
//  Copyright © 2018 Blind Chameleon Studio. All rights reserved.
//

import UIKit

protocol ChosenDicesDelegate {
    
    func userSelectDice( dices: Array<DiceType> )
    
}

class DiceCatalogVC: UIViewController {
    
    var delegate : ChosenDicesDelegate?
    
    // View variables
    var d4Count : Int = 0
    var d6Count : Int = 2
    var d8Count : Int = 0
    var d10Count : Int = 0
    var d12Count : Int = 0
    var d14Count : Int = 0
    var d16Count : Int = 0
    var d20Count : Int = 0
    var dPercentCount : Int = 0
    let MAX_DICES : Int = 6
    var totalDiceCount : Int = 0
    var diceArray : Array<DiceType> = [DiceType.Dx6, DiceType.Dx6]
    
    // Linked minus buttons
    @IBOutlet weak var d4minusButton: UIButton! // Has TAG = 1
    @IBOutlet weak var d6minusButton: UIButton! // Has TAG = 3
    @IBOutlet weak var d8minusButton: UIButton! // Has TAG = 5
    @IBOutlet weak var d10minusButton: UIButton! // Has TAG = 7
    @IBOutlet weak var d12minusButton: UIButton! // Has TAG = 9
    @IBOutlet weak var d14minusButton: UIButton! // Has TAG = 11
    @IBOutlet weak var d16minusButton: UIButton! // Has TAG = 13
    @IBOutlet weak var d20minusButton: UIButton! // Has TAG = 15
    @IBOutlet weak var dPercentMinusButton: UIButton! // Has TAG = 17
    
    // Linked plus buttons
    @IBOutlet weak var d4plusButton: UIButton! // Has TAG = 2
    @IBOutlet weak var d6plusButton: UIButton! // Has TAG = 4
    @IBOutlet weak var d8plusButton: UIButton! // Has TAG = 6
    @IBOutlet weak var d10plusButton: UIButton! // Has TAG = 8
    @IBOutlet weak var d12plusButton: UIButton! // Has TAG = 10
    @IBOutlet weak var d14plusButton: UIButton! // Has TAG = 12
    @IBOutlet weak var d16plusButton: UIButton! // Has TAG = 14
    @IBOutlet weak var d20plusButton: UIButton! // Has TAG = 16
    @IBOutlet weak var dPercentPlusButton: UIButton! // Has TAG = 18
    
    // Linked dice count labels
    @IBOutlet weak var d4countLabel: UILabel!
    @IBOutlet weak var d6countLabel: UILabel!
    @IBOutlet weak var d8countLabel: UILabel!
    @IBOutlet weak var d10countLabel: UILabel!
    @IBOutlet weak var d12countLabel: UILabel!
    @IBOutlet weak var d14countLabel: UILabel!
    @IBOutlet weak var d16countLabel: UILabel!
    @IBOutlet weak var d20countLabel: UILabel!
    @IBOutlet weak var dPercentCountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide minus buttons on load
        d4minusButton.isHidden = true
        d8minusButton.isHidden = true
        d10minusButton.isHidden = true
        d12minusButton.isHidden = true
        d14minusButton.isHidden = true
        d16minusButton.isHidden = true
        d20minusButton.isHidden = true
        dPercentMinusButton.isHidden = true
        
        d6countLabel.text = "\(d6Count)"
    }
    
    // Minus Button Logic
    @IBAction func minusPressed(_ sender: UIButton) {
        
        if sender.tag == 1 {
            print("d4 -")
            d4Count -= 1
            ifCountZeroHideButton(diceCount: d4Count, countLabel: d4minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx4)
            totalDiceCount -= 1
            d4countLabel.text = "\(d4Count)"
        }
        else if sender.tag == 3 {
            print("d6 -")
            d6Count -= 1
            ifCountZeroHideButton(diceCount: d6Count, countLabel: d6minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx6)
            totalDiceCount -= 1
            d6countLabel.text = "\(d6Count)"
        }
        else if sender.tag == 5 {
            print("d8 -")
            d8Count -= 1
            ifCountZeroHideButton(diceCount: d8Count, countLabel: d8minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx8)
            totalDiceCount -= 1
            d8countLabel.text = "\(d8Count)"
        }
        else if sender.tag == 7 {
            print("d10 -")
            d10Count -= 1
            ifCountZeroHideButton(diceCount: d10Count, countLabel: d10minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx10)
            totalDiceCount -= 1
            d10countLabel.text = "\(d10Count)"
        }
        else if sender.tag == 9 {
            print("d12 -")
            d12Count -= 1
            ifCountZeroHideButton(diceCount: d12Count, countLabel: d12minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx12)
            totalDiceCount -= 1
            d12countLabel.text = "\(d12Count)"
        }
        else if sender.tag == 11 {
            print("d14 -")
            d14Count -= 1
            ifCountZeroHideButton(diceCount: d14Count, countLabel: d14minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx14)
            totalDiceCount -= 1
            d14countLabel.text = "\(d14Count)"
        }
        else if sender.tag == 13 {
            print("d16 -")
            d16Count -= 1
            ifCountZeroHideButton(diceCount: d16Count, countLabel: d16minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx16)
            totalDiceCount -= 1
            d16countLabel.text = "\(d16Count)"
        }
        else if sender.tag == 15 {
            print("d20 -")
            d20Count -= 1
            ifCountZeroHideButton(diceCount: d20Count, countLabel: d20minusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.Dx20)
            totalDiceCount -= 1
            d20countLabel.text = "\(d20Count)"
        }
        else if sender.tag == 17 {
            print("d% -")
            dPercentCount -= 1
            ifCountZeroHideButton(diceCount: dPercentCount, countLabel: dPercentMinusButton)
            deleteDiceFromArray(diceArray: diceArray, diceToRemove: DiceType.DxPercent)
            totalDiceCount -= 1
            dPercentCountLabel.text = "\(dPercentCount)"
        }
        updatePlusButtonVisibility(numberOfDices: totalDiceCount)
        
    }
    
    // Plus Button Logic
    @IBAction func plusPressed(_ sender: UIButton) {
        
        if sender.tag == 2 {
            print("d4 +")
            d4Count += 1
            ifCountZeroHideButton(diceCount: d4Count, countLabel: d4minusButton)
            diceArray += [DiceType.Dx4]
            totalDiceCount += 1
            d4countLabel.text = "\(d4Count)"
        }
        else if sender.tag == 4 {
            print("d6 +")
            d6Count += 1
            ifCountZeroHideButton(diceCount: d6Count, countLabel: d6minusButton)
            diceArray += [DiceType.Dx6]
            totalDiceCount += 1
            d6countLabel.text = "\(d6Count)"
        }
        else if sender.tag == 6 {
            print("d8 +")
            d8Count += 1
            ifCountZeroHideButton(diceCount: d8Count, countLabel: d8minusButton)
            diceArray += [DiceType.Dx8]
            totalDiceCount += 1
            d8countLabel.text = "\(d8Count)"
        }
        else if sender.tag == 8 {
            print("d10 +")
            d10Count += 1
            ifCountZeroHideButton(diceCount: d10Count, countLabel: d10minusButton)
            diceArray += [DiceType.Dx10]
            totalDiceCount += 1
            d10countLabel.text = "\(d10Count)"
        }
        else if sender.tag == 10 {
            print("d12 +")
            d12Count += 1
            ifCountZeroHideButton(diceCount: d12Count, countLabel: d12minusButton)
            diceArray += [DiceType.Dx12]
            totalDiceCount += 1
            d12countLabel.text = "\(d12Count)"
        }
        else if sender.tag == 12 {
            print("d14 +")
            d14Count += 1
            ifCountZeroHideButton(diceCount: d14Count, countLabel: d14minusButton)
            diceArray += [DiceType.Dx14]
            totalDiceCount += 1
            d14countLabel.text = "\(d14Count)"
        }
        else if sender.tag == 14 {
            print("d16 +")
            d16Count += 1
            ifCountZeroHideButton(diceCount: d16Count, countLabel: d16minusButton)
            diceArray += [DiceType.Dx16]
            totalDiceCount += 1
            d16countLabel.text = "\(d16Count)"
        }
        else if sender.tag == 16 {
            print("d20 +")
            d20Count += 1
            ifCountZeroHideButton(diceCount: d20Count, countLabel: d20minusButton)
            diceArray += [DiceType.Dx20]
            totalDiceCount += 1
            d20countLabel.text = "\(d20Count)"
        }
        else if sender.tag == 18 {
            print("d% +")
            dPercentCount += 1
            ifCountZeroHideButton(diceCount: dPercentCount, countLabel: dPercentMinusButton)
            diceArray += [DiceType.DxPercent]
            totalDiceCount += 1
            dPercentCountLabel.text = "\(dPercentCount)"
        }
        updatePlusButtonVisibility(numberOfDices: totalDiceCount)
    }
    
    // If total number of dices equals max allowed number of dices, hide plus button
    func updatePlusButtonVisibility( numberOfDices : Int ) {
        if numberOfDices == MAX_DICES {
            d4plusButton.isHidden = true
            d6plusButton.isHidden = true
            d8plusButton.isHidden = true
            d10plusButton.isHidden = true
            d12plusButton.isHidden = true
            d14plusButton.isHidden = true
            d16plusButton.isHidden = true
            d20plusButton.isHidden = true
            dPercentPlusButton.isHidden = true
        }
        else {
            d4plusButton.isHidden = false
            d6plusButton.isHidden = false
            d8plusButton.isHidden = false
            d10plusButton.isHidden = false
            d12plusButton.isHidden = false
            d14plusButton.isHidden = false
            d16plusButton.isHidden = false
            d20plusButton.isHidden = false
            dPercentPlusButton.isHidden = false
        }
    }
    
    // If number of dices equals zero, hide minus button
    func ifCountZeroHideButton( diceCount : Int, countLabel : UIButton){
        if diceCount == 0 {
            countLabel.isHidden = true
        } else {
            countLabel.isHidden = false
        }
    }
    
    // Find index of selected dice and delete it
    func deleteDiceFromArray( diceArray : Array<DiceType>, diceToRemove : DiceType) {
        let indexOfDeleted = diceArray.index(of: diceToRemove)
        self.diceArray.remove(at: indexOfDeleted!)
        
    }
    
    // Save button functionality
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        print(diceArray)
    }
    

}
